﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using TestSupport;

namespace SpecFlowTests
{
    [Binding]
    public class Steps
    {
        [Given(@"I am on the ""(.*)"" page")]
        public void GivenIAmOnThePage(string p0)
        {
            var brands = BrandSupport.SetUpBrands();
            Browser.Current.Driver.Navigate().GoToUrl(brands.GetBaseUrl(Configuration.Brand));
        }

        [When(@"I press ""(.*)"" button")]
        public void WhenIPressButton(string p0)
        {
            Browser.Current.Driver.FindElement(By.Id("continueButton") ).Click();
        }

        [Then(@"the validation summary should be seen on the screen")]
        public void ThenTheValidationSummaryShouldBeSeenOnTheScreen()
        {
            var webElement = Browser.Current.Driver.FindElement(By.CssSelector("largeErrorMessage"));
            Assert.That(webElement.Text,Does.Contain("have not been completed or have been filled incorrectly"));
        }


        [Given(@"I fill the details in the page")]
        public void GivenIFillTheDetailsInThePage()
        {
            var driver = Browser.Current.Driver;
            var titleDropDown = driver.FindElement(By.Id("input_1-1"));
            SelectDropDownByText(titleDropDown, "Mr");

            driver.FindElement(By.Id("input_1-2")).SendKeys("FistName");
            driver.FindElement(By.Id("input_1-3")).SendKeys("surname");


            var dayDD = driver.FindElement(By.Id("input_1-4_d"));
            var monthDD = driver.FindElement(By.Id("input_1-4_m"));
            var yearDD = driver.FindElement(By.Id("input_1-4_y"));

            SelectDropDownByText(dayDD, "01");
            SelectDropDownByText(monthDD, "Jan");
            SelectDropDownByText(yearDD, "1950");


            driver.FindElement(By.Id("input_1-5_house")).SendKeys("1");
            driver.FindElement(By.Id("input_1-5_pcode")).SendKeys("pe25rb");
            driver.FindElement(By.Id("findAddressButton_1-5")).Click();

            driver.FindElement(By.Id("input_1-6")).SendKeys("A1");


        }
        [When(@"I wait for (.*) seconds")]
        public void WhenIWaitForSeconds(int p0)
        {
          Thread.Sleep(new TimeSpan(0,0,0,p0));
        }

        [Then(@"""(.*)"" page should be seen")]
        public void ThenPageShouldBeSeen(string p0)
        {
            Assert.That(Browser.Current.Driver.Title, Is.StringContaining(p0));
        }

        private void SelectDropDownByText(IWebElement element, string text)
        {
            new SelectElement(element).SelectByText(text);
        }
    }
}
