﻿@Parallel @test
Feature: Welcome page test feature
	Test welcomepage contents and functionality
		
Scenario: Welcome page validation
	Given I am on the "Welcome" page	
	When I press "Continue" button	
	Then the validation summary should be seen on the screen


	Scenario: Welcome page validation after filling the page
	Given I am on the "Welcome" page	
	And I fill the details in the page
	When I press "Continue" button	
	And I wait for 10 seconds
	Then "About you" page should be seen