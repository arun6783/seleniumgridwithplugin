﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using TestSupport;

namespace SpecFlowTests
{
    [TestFixture]
    public class JourneyInitialisation
    {
        [OneTimeSetUp]
        public void TestFixtureSetup()
        {
            try
            {
                IWebDriver remoteWebDriver = new RemoteWebDriver(
                    new Uri(string.Format("http://{0}:{1}/wd/hub", Configuration.HostIp, Configuration.Port)),
                    Configuration.DesiredCapabilities);
                Browser.Initialise(remoteWebDriver);
            }
            catch (Exception ex)
            {
                Browser.Initialise(new FirefoxDriver());
            }
        }


        [OneTimeTearDown]
        public void FixtureTearDown()
        {
            Browser.Close();
        }
    }
}
