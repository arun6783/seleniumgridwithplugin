﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestSupport;

namespace TestSupport
{
    public class Halifax : Brand
    {
        public override string BaseUrl { get; set; }

        public override string Name { get; set; }
    }
}
