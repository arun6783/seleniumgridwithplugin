﻿using TestSupport;

namespace TestSupport
{
    public class O2 : Brand
    {
        public override string BaseUrl { get; set; }

        public override string Name { get; set; }
    }
}