using System;
using System.Collections.Generic;
using System.Linq;

namespace TestSupport
{
    public static class BrandSupport
    {
        public static IEnumerable<Brand> SetUpBrands()
        {
            return new List<Brand>
            {
                new Halifax
                {
                    BaseUrl = "https://halifaxcarinsurance.insure-systems.co.uk/Car/HX01/NLE",
                    Name = "Halifax"
                },
                new Lloyds
                {
                    BaseUrl = "https://lloydsbankcarinsurance.insure-systems.co.uk/Car/LY01/NLE",
                    Name = "Lloyds"
                },
                new O2 {BaseUrl = "https://drive.o2.co.uk/o2drive/o201/NLE", Name = "O2"},
                new PostOffice {BaseUrl = "https://postoffice.insure-systems.co.uk/car/PO01/NLE", Name = "PostOffice"}
            };
        }

        public static string GetBaseUrl(this IEnumerable<Brand> brands, string brandName)
        {
            brands =  brands ?? SetUpBrands();
            var brand = brands.FirstOrDefault(x => x.Name == brandName);
            if (brand != null) return brand.BaseUrl;

            throw new ArgumentException("Invalid brand", brandName);
        }
    }
}