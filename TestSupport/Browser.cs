﻿using OpenQA.Selenium;

namespace TestSupport
{
    public class Browser
    {
        private Browser(IWebDriver driver)
        {
            Driver = driver;
        }

        public IWebDriver Driver { get; private set; }

        public static void Initialise(IWebDriver remoteWebDriver)
        {
            Current = new Browser(remoteWebDriver);
        }

        public static Browser Current { get; private set; }

        public static void Close()
        {
            Current.Driver.Quit();
        }
    }
}