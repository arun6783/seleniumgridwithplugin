﻿using TestSupport;

namespace TestSupport
{
    public class Lloyds : Brand
    {
        public override string BaseUrl { get; set; }

        public override string Name { get; set; }
    }
}