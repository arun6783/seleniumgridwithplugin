﻿using TestSupport;

namespace TestSupport
{
    public class PostOffice : Brand
    {
        public override string BaseUrl { get; set; }

        public override string Name { get; set; }
    }
}