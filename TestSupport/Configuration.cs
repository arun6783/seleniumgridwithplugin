﻿using System;
using OpenQA.Selenium;

namespace TestSupport
{
    public static class Configuration
    {
        public static string SharedVolume
        {
            get
            {
                var path = @"\\AARIKHA\testresults";
                return path;
            }
        }


        public static string Brand
        {
            get
            {
                var brand = Environment.GetEnvironmentVariable("BRAND") ?? "O2";
                return brand;
            }
        }


        public static string HostIp
        {
            get
            {
                var hostIp = Environment.GetEnvironmentVariable("HOST_IP");
                return hostIp != null ? hostIp : "192.168.99.100"; //"52.19.94.58";
            }
        }

        public static string Port
        {
            get
            {
                var port = Environment.GetEnvironmentVariable("CHROME_PORT");
                return port != null ? port : "4444";
            }
        }

        public static ICapabilities DesiredCapabilities
        {
            get
            {
                var desiredBrowser = Environment.GetEnvironmentVariable("BROWSER");

                switch (desiredBrowser)
                {
                    case "Chrome":
                        return OpenQA.Selenium.Remote.DesiredCapabilities.Chrome();
                    case "Firefox":
                        return OpenQA.Selenium.Remote.DesiredCapabilities.Firefox();
                    case "IE":
                        return OpenQA.Selenium.Remote.DesiredCapabilities.InternetExplorer();
                    case "Phantom":
                        return OpenQA.Selenium.Remote.DesiredCapabilities.PhantomJS();

                    default:
                        return OpenQA.Selenium.Remote.DesiredCapabilities.Chrome();
                }
            }
        }
    }
}