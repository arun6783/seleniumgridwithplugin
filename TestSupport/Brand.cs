﻿namespace TestSupport
{
    public abstract class Brand
    {
        public abstract string BaseUrl { get; set; }
        public abstract string Name { get; set; }
    }
}
