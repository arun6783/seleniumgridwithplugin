using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using TestSupport;

namespace SeleniumGrid.Tests
{
    [TestFixture]
    [Parallelizable]
    [Category("Grid")]
    public class HalifaxJourneyTest 
    {
        private RemoteWebDriver _remoteWebDriver;
        private List<Brand> _brands;


        [SetUp]
        public void SetUp()
        {
            _brands = BrandSupport.SetUpBrands().ToList();
            try
            {
                _remoteWebDriver = new RemoteWebDriver(
                    new Uri(string.Format("http://{0}:{1}/wd/hub", Configuration.HostIp, Configuration.Port)),
                    Configuration.DesiredCapabilities);
            }
            catch (Exception ex)
            {
                _remoteWebDriver = new FirefoxDriver();
            }
        }

        private void SelectDropDownByText(IWebElement element, string text)
        {
            new SelectElement(element).SelectByText(text);
        }
        [TearDown]
        public void TearDown()
        {
            try
            {
                var fileName = TestContext.CurrentContext.Test.Name;
                var testResultPath = Path.Combine(Configuration.SharedVolume, Configuration.Brand);
                var fileFullPath = Path.Combine(testResultPath, string.Format("{0}.jpg", fileName));

                if (!Directory.Exists(testResultPath))
                {
                    Directory.CreateDirectory(testResultPath);
                }
                ((ITakesScreenshot)_remoteWebDriver).GetScreenshot().SaveAsFile(fileFullPath, ImageFormat.Jpeg);
            }
            catch (Exception)
            {
            }
            _remoteWebDriver.Quit();
        }


        [Test]
        public void Should_Be_Able_To_Click_Next_In_Welcome_Page()
        {
            var brandName = "Halifax";
            var baseUrl = _brands.GetBaseUrl(brandName);

            _remoteWebDriver.Navigate().GoToUrl(baseUrl);
            var titleDropDown = _remoteWebDriver.FindElement(By.Id("input_1-1"));
            SelectDropDownByText(titleDropDown, "Mr");

            _remoteWebDriver.FindElement(By.Id("input_1-2")).SendKeys("FistName");
            _remoteWebDriver.FindElement(By.Id("input_1-3")).SendKeys("surname");


            var dayDD = _remoteWebDriver.FindElement(By.Id("input_1-4_d"));
            var monthDD = _remoteWebDriver.FindElement(By.Id("input_1-4_m"));
            var yearDD = _remoteWebDriver.FindElement(By.Id("input_1-4_y"));

            SelectDropDownByText(dayDD, "01");
            SelectDropDownByText(monthDD, "Jan");
            SelectDropDownByText(yearDD, "1950");


            _remoteWebDriver.FindElement(By.Id("input_1-5_house")).SendKeys("1");
            _remoteWebDriver.FindElement(By.Id("input_1-5_pcode")).SendKeys("pe25rb");
            _remoteWebDriver.FindElement(By.Id("findAddressButton_1-5")).Click();

            _remoteWebDriver.FindElement(By.Id("input_1-6")).SendKeys("A1");


            _remoteWebDriver.FindElement(By.ClassName("continueButton")).Click();

            Thread.Sleep(5000);

            Assert.That(_remoteWebDriver.Title, Is.StringContaining("About you"));
        }
    }
}