﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using TestSupport;

namespace SeleniumGrid.Tests
{
    [Category("Containers")]
    [TestFixture]
    public class SimpleRemoteTest
    {
        private IWebDriver _remoteWebDriver;
        private IEnumerable<Brand> _brands;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            try
            {
                _brands = BrandSupport.SetUpBrands();

                _remoteWebDriver = new RemoteWebDriver(new Uri(string.Format("http://{0}:{1}/wd/hub", Configuration.HostIp, Configuration.Port)),
                       DesiredCapabilities.Chrome());
            }
            catch (Exception ex)
            {
                _remoteWebDriver = new FirefoxDriver();
            }
        }

        [SetUp]
        public void SetUp()
        {
            _remoteWebDriver.Navigate().GoToUrl("http://www.google.co.uk");
        }

        [Test]
        public void Should_Check_HomePage_loaded()
        {
            var logoElement = _remoteWebDriver.FindElement(By.Id("hplogo"));

            Assert.That(logoElement, Is.Not.Null);
            Assert.That(logoElement.Text, Is.EqualTo("UK"));
        }

        [Test]
        public void Should_Search_For_Docker()
        {
            _remoteWebDriver.FindElement(By.Name("q")).SendKeys("Docker");
            _remoteWebDriver.FindElement(By.Name("btnG")).Click();
            Thread.Sleep(5000);
            var elements = _remoteWebDriver.FindElements(By.Id("res"));
            Assert.That(elements, Is.Not.Null);
            Assert.That(elements[0].Text, Is.StringContaining("Docker - Build, Ship, and Run Any App, Anywhere"));
        }

        [Test]
        public void Should_Be_Able_To_Click_Next_In_Welcome_Page()
        {
            var brandName = Configuration.Brand;
            var baseUrl = _brands.GetBaseUrl(brandName);

            _remoteWebDriver.Navigate().GoToUrl(baseUrl);
            var titleDropDown = _remoteWebDriver.FindElement(By.Id("input_1-1"));
            SelectDropDownByText(titleDropDown, "Mr");

            _remoteWebDriver.FindElement(By.Id("input_1-2")).SendKeys("FistName");
            _remoteWebDriver.FindElement(By.Id("input_1-3")).SendKeys("surname");


            var dayDD = _remoteWebDriver.FindElement(By.Id("input_1-4_d"));
            var monthDD = _remoteWebDriver.FindElement(By.Id("input_1-4_m"));
            var yearDD = _remoteWebDriver.FindElement(By.Id("input_1-4_y"));

            SelectDropDownByText(dayDD, "01");
            SelectDropDownByText(monthDD, "Jan");
            SelectDropDownByText(yearDD, "1950");


            _remoteWebDriver.FindElement(By.Id("input_1-5_house")).SendKeys("1");
            _remoteWebDriver.FindElement(By.Id("input_1-5_pcode")).SendKeys("pe25rb");
            _remoteWebDriver.FindElement(By.Id("findAddressButton_1-5")).Click();

            _remoteWebDriver.FindElement(By.Id("input_1-6")).SendKeys("A1");

            //click continue button

            _remoteWebDriver.FindElement(By.ClassName("continueButton")).Click();

            Thread.Sleep(5000);

            Assert.That(_remoteWebDriver.Title, Is.StringContaining("About you"));
        }

        [TearDown]
        public void TearDown()
        {
            var fileName = TestContext.CurrentContext.Test.Name;
            var testResultPath = Path.Combine(Configuration.SharedVolume, Configuration.Brand);
            var fileFullPath = Path.Combine(testResultPath, string.Format("{0}.jpg", fileName));

            if (!Directory.Exists(testResultPath))
            {
                Directory.CreateDirectory(testResultPath);
            }
            ((ITakesScreenshot)_remoteWebDriver).GetScreenshot().SaveAsFile(fileFullPath, ImageFormat.Jpeg);
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            _remoteWebDriver.Quit();
        }


        private void SelectDropDownByText(IWebElement element, string text)
        {
            new SelectElement(element).SelectByText(text);
        }
    }
}
