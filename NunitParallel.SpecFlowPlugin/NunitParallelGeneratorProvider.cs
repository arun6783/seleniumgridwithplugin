﻿using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NunitParallel.SpecFlowPlugin;
using TechTalk.SpecFlow.Generator;
using TechTalk.SpecFlow.Generator.UnitTestProvider;
using TechTalk.SpecFlow.Infrastructure;
using TechTalk.SpecFlow.Utils;

[assembly: GeneratorPlugin(typeof(NunitParallelGeneratorPlugin))]
namespace NunitParallel.SpecFlowPlugin
{
   
    public class NunitParallelGeneratorProvider : NUnitTestGeneratorProvider
    {
       
        public NunitParallelGeneratorProvider(CodeDomHelper codeDomHelper)
            : base(codeDomHelper)
        {
        }

      
        public override void SetTestClass(TestClassGenerationContext generationContext, string featureTitle,
            string featureDescription)
        {
            base.SetTestClass(generationContext, featureTitle, featureDescription);
            foreach (CodeAttributeDeclaration declaration in generationContext.TestClass.CustomAttributes)
            {
                if (declaration.Name == "Parallelizable")
                {
                    generationContext.TestClass.CustomAttributes.Remove(declaration);
                }
            }

            if (generationContext.Document.SpecFlowFeature.Tags.Any(x => x.Name.Contains("Parallel")))
                generationContext.TestClass.CustomAttributes.Add(
                    new CodeAttributeDeclaration(new CodeTypeReference("NUnit.Framework.ParallelizableAttribute")));
        }

        public override void SetTestClassCategories(TestClassGenerationContext generationContext, IEnumerable<string> featureCategories)
        {
            var categories = featureCategories.ToList();
            if (categories.Contains("Parallel"))
            {
                categories.Remove("Parallel");
            }
            base.SetTestClassCategories(generationContext, categories);
          
        }
    }
}
