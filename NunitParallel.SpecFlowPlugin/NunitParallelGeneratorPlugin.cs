﻿using System.Diagnostics;
using BoDi;
using TechTalk.SpecFlow.Generator.Plugins;
using TechTalk.SpecFlow.Generator.UnitTestProvider;

namespace NunitParallel.SpecFlowPlugin
{
    public class NunitParallelGeneratorPlugin : IGeneratorPlugin
    {
        public void RegisterDependencies(ObjectContainer container)
        {
        }

        public void RegisterCustomizations(ObjectContainer container)
        {
            container.RegisterTypeAs<NunitParallelGeneratorProvider, IUnitTestGeneratorProvider>("nunit.custom");
        }

        public void RegisterConfigurationDefaults()
        {
        }

        public void Initialize(GeneratorPluginEvents generatorPluginEvents,
            GeneratorPluginParameters generatorPluginParameters)
        {
            
            generatorPluginEvents.RegisterDependencies +=
                (sender, args) => { RegisterCustomizations(args.ObjectContainer); };
        }
    }
}