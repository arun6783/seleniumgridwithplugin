FROM mono:3.12.0-onbuild

RUN mkdir -p /usr/testResults

CMD ["mono", "/usr/src/app/source/packages/NUnit.ConsoleRunner.3.4.1/tools/nunit3-console.exe", "SeleniumGrid.Tests.dll"]
